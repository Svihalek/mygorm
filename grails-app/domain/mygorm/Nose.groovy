package mygorm

class Nose {
    static belongsTo = [face: Face]

    String type

    static constraints = {
        face nullable:false
    }
}
