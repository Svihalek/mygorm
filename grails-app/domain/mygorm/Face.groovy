package mygorm

class Face extends Parent{

    String color
    Integer age
    Nose nose

    static constraints = {
        age min:0, max:110
        color(inList: ["Black", "White", "Yellow"])
        nose nullable: true
    }
}
